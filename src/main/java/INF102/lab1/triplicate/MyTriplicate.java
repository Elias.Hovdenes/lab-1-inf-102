package INF102.lab1.triplicate;

import java.util.List;

public class MyTriplicate<T> implements ITriplicate<T> {

    @Override
    public T findTriplicate(List<T> list) {
        int n = list.size();
		for (int i = 0; i < n; i++) {
            for (int j = i+1; j < n; j++) {
                if (list.get(i).equals(list.get(j))){
                    for (int k = j+1; k < n; k++) {
                        if (list.get(k).equals(list.get(i))){
                            return list.get(k);
                        }
                    }
                }
            }	
		}
        
		return null;
    }
    
}
